from shutil import copy
import glob
import os
from addict import Dict
import json
from tqdm import tqdm

def config_reader(path: str) -> Dict:
    with open(path) as f:
        cfg = json.load(f)
    return Dict(cfg)

def read_coco(path,filename):
    '''
    :param path: path of your json file
    :param filename: name of json file with '.json' extension.(example='train.json')
    :return:
    '''
    basePath = os.path.join(path)
    coco = config_reader(os.path.join(basePath, filename))
    # print("read it")
    return coco

def save_coco_file(coco,filename='sample'):
    with open(filename+'.json', "w") as fp:
        json.dump(coco, fp)

def create_json(coco, im_list):
    '''
    the function creating coco
    file for the given image name list.
    :param coco: main coco file
    :param im_list: List of images,
    that will be created annotations
    :return: annotations file
    '''
    ims = []
    ids = []
    ann_items = []
    for im in coco['images']:
        if im['file_name'] in im_list:
            ids.append(im.get('id'))
            ims.append(im)

    for ann in coco['annotations']:
        if ann["image_id"] in ids:
            ann_items.append(ann)
    coco['images'] = ims
    coco['annotations'] = ann_items
    return coco

def sperate_folders(input_path,input_folder,main_path,x=400):
    '''
    Seperating images and annotations to folder.
    You can select for seperate number of datas.
    :param input_path: Image path
    :param input_folder: Image Folder
    :param main_path:  Path of the annotations
    :param x: Choose the number of images to saving folder.
    :return:
    '''
    a = 0
    im_list = []
    length = len((glob.glob(os.path.join(input_path, input_folder, '*.jpeg')))) +\
             len((glob.glob(os.path.join(input_path, input_folder, '*.jpg'))))
    project_files = (glob.glob(os.path.join(input_path, input_folder, '*.jpeg'))) +\
                    (glob.glob(os.path.join(input_path, input_folder, '*.jpg')))
    for index, img_path in tqdm(enumerate(project_files)):
        dst = main_path + '/parttie' + str(a)
        if not os.path.exists(dst):
            os.mkdir(dst)
        im_list.append(os.path.basename(img_path))
        copy(img_path,dst)
        if index%x == 0 and not index == 0 or index == length-1:
            a = a + 1
            coco = read_coco(main_path, 'merged.json')
            save_coco_file(create_json(coco,im_list),'coco_filler'+str(a-1))
            im_list = []

sperate_folders(input_path='/home/izzet/Downloads/configuration-2022-01-13-07-44-24/',
                input_folder='merged',main_path='/home/izzet/Desktop/projects/merge/',x=1000)

# sperate_folders(input_path='/home/izzet/Desktop/',
#                  input_folder='big_data',main_path='/home/izzet/Desktop/pole',x=1000)